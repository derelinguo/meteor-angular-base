// Home Controller

var home = angular.module('Home');

home.controller('HomeController', ['$rootScope', function($rootScope) {
    $rootScope.title = 'Hello World!';
}]);
