// Configuration for home. Defines angular-ui-router states
var home = angular.module('Home', ['common']);

home.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('home', {
        url: '',
        templateUrl: 'home.html',
        controller: 'HomeController'
    });
}]);
