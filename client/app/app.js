var app = angular.module('meteor-angular-base', ['Home', 'ui.bootstrap']);

app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
