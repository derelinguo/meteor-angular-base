# meteor-angular-base
Clean self describing meteor base with basic angular app and heroku support

## Getting running
1. Ensure you have the [Heroku toolbelt](https://toolbelt.heroku.com/) and
[meteor](https://www.meteor.com/install) installed.

2. Ensure [MongoDB](http://docs.mongodb.org/manual/installation/) is installed and running

3. Start with foreman
```
$ foreman start --env dev.env
```
